Pi Scheduler
=============
Pi Scheduler is REST service to schedule/cancel/run  job on demand. Pi Scheduler will call the respective service (using the HTTP post) to execute a task at scheduled time.  Pi Scheduler (in general) expects HTTP ACCEPT code in response. 


Architecture
============
![Architecture](architecture.jpg)


Building the application
========================
`./mvnw clean package`



Running the application
========================
`./mvnw spring-boot:run`


API
===

**Creating a new job**

```

curl -XPOST 'http://localhost:8080/jobs' -H 'Content-Type: application/json' -d'
{
	"name": "Send payslips",
	"cronExpression": "*/3 * * * * *",
	"url": "http://my-payroll-system/send/payslips"
}
'


```

**Cancel job**

```

curl -XDELETE 'http://localhost:8080/jobs/123456' 


```


**Execute job on demand**

```

curl -XPOST 'http://localhost:8080/jobs/123456/runNow' 


```



