package net.p.i.scheduler;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.tomakehurst.wiremock.junit.WireMockRule;
import net.p.i.scheduler.domain.Job;
import net.p.i.scheduler.requests.JobRequest;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static com.github.tomakehurst.wiremock.client.WireMock.*;
import static com.github.tomakehurst.wiremock.core.WireMockConfiguration.options;
import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;
import static org.springframework.http.HttpMethod.DELETE;
import static org.springframework.http.HttpMethod.POST;
import static org.springframework.http.HttpStatus.*;


@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment= RANDOM_PORT)
public class JobsControllerIntegrationTests {

    @Autowired
    private TestRestTemplate restTemplate;

    @Rule
    public WireMockRule wireMockRule = new WireMockRule(options().port(10001));


    @Test
    public void should_return_errors_given_request_is_invalid(){
        //When
        ResponseEntity<List> responseEntity = restTemplate.exchange("/jobs", POST, new HttpEntity<>(new JobRequest()), List.class);
        //Then
        assertNotNull(responseEntity);
        assertThat(responseEntity.getStatusCode(), is(BAD_REQUEST));
        assertThat((List<String>)responseEntity.getBody(), containsInAnyOrder(
                "Name of the job is required",
                "Cron schedule is required",
                "URL is required"));

        //When URL is invalid
        JobRequest invalidUrlRequest = newJobRequest("test", "invalid-url", "*/5 * * * * *");
        responseEntity = restTemplate.exchange("/jobs", POST, new HttpEntity<>(invalidUrlRequest), List.class);
        assertThat((List<String>)responseEntity.getBody(), contains("URL is invalid"));
    }

    @Test
    public void should_create_new_cron_job() throws JsonProcessingException {
        JobRequest request = newJobRequest("test", "http://127.0.0.1:10001/doSomething", "*/3 * * * * *");
        //When
        ResponseEntity<Job> entity = restTemplate.exchange("/jobs", POST, new HttpEntity<>(request), Job.class);
        //Then verify that Job is successfully created
        assertNotNull(entity);
        assertThat(entity.getStatusCode(), is(CREATED));
        //Then verify that scheduled job is called after 3 second
        waitForSeconds(3);
        verify(1, postRequestedFor(urlEqualTo("/doSomething")));
    }

    @Test
    public void should_return_not_found_when_cancelling_job_that_does_not_exist(){
        //When
        ResponseEntity<String> responseEntity = restTemplate.exchange(
                "/jobs/0000", DELETE, HttpEntity.EMPTY, String.class);
        //Then
        assertNotNull(responseEntity);
        assertThat(responseEntity.getStatusCode(), is(NOT_FOUND));
    }

    @Test
    public void should_cancel_job(){
        JobRequest request = newJobRequest("test for cancellation", "http://127.0.0.1:10001/doSomething", "*/50 * * * * *");
        ResponseEntity<Job> jobEntity = restTemplate.exchange("/jobs", POST, new HttpEntity<>(request), Job.class);
        //verify that Job is successfully created
        assertNotNull(jobEntity);
        assertThat(jobEntity.getStatusCode(), is(CREATED));

        //When
        ResponseEntity<String> deleteResponse = restTemplate.exchange(
                "/jobs/"+jobEntity.getBody().getId(), DELETE, HttpEntity.EMPTY, String.class);

        //Then
        assertNotNull(deleteResponse);
        assertThat(deleteResponse.getStatusCode(), is(OK));
        verify(0, postRequestedFor(urlEqualTo("/doSomething")));
    }

    @Test
    public void should_return_not_found_when_running_a_job_that_does_not_exist(){
        //When
        ResponseEntity<String> responseEntity = restTemplate.exchange(
                "/jobs/0000/runNow", POST, HttpEntity.EMPTY, String.class);
        //Then
        assertNotNull(responseEntity);
        assertThat(responseEntity.getStatusCode(), is(NOT_FOUND));
    }

    @Test
    public void should_run_job_on_demand(){
        //Given
        JobRequest request = newJobRequest("test for runnow", "http://127.0.0.1:10001/doSomething", "* */50 * * * *");
        ResponseEntity<Job> jobEntity = restTemplate.exchange("/jobs", POST, new HttpEntity<>(request), Job.class);
        //verify that Job is successfully created
        assertNotNull(jobEntity);
        assertThat(jobEntity.getStatusCode(), is(CREATED));

        //When
        ResponseEntity<String> responseEntity = restTemplate.exchange(
                "/jobs/"+jobEntity.getBody().getId()+"/runNow", POST, HttpEntity.EMPTY, String.class);
        //Then
        assertNotNull(responseEntity);
        assertThat(responseEntity.getStatusCode(), is(ACCEPTED));
        waitForSeconds(3);
        verify(1, postRequestedFor(urlEqualTo("/doSomething")));
    }


    private static void waitForSeconds(int seconds){
        try {
            Thread.sleep(seconds * 1000);
        } catch (InterruptedException e) {
            //ignore
        }
    }

    private static JobRequest newJobRequest(String name, String url, String cronExpression){
        JobRequest request = new JobRequest();
        request.setName(name);
        request.setUrl(url);
        request.setCronExpression(cronExpression);
        return request;
    }
}