package net.p.i.scheduler.tasks.builders;

import net.p.i.scheduler.tasks.PostHttpJob;

public class PostHttpJobBuilder {

    private String id;
    private String name;
    private String url;

    public PostHttpJob build(){
        return new PostHttpJob(id, name, url);
    }

    public PostHttpJobBuilder id(String id){
        this.id = id;
        return this;
    }

    public PostHttpJobBuilder name(String name){
        this.name = name;
        return this;
    }

    public PostHttpJobBuilder url(String url){
        this.url = url;
        return this;
    }
}
