package net.p.i.scheduler.tasks;

import org.apache.http.HttpResponse;
import org.slf4j.Logger;

import java.io.IOException;

import static org.apache.http.client.fluent.Request.Post;
import static org.slf4j.LoggerFactory.getLogger;

public class PostHttpJob implements Runnable{

    private static final Logger logger = getLogger(PostHttpJob.class);

    private final String id;
    private final String name;
    private final String url;

    public PostHttpJob(String id, String name, String url) {
        this.id = id;
        this.name = name;
        this.url = url;
    }

    @Override
    public void run() {
        try {
            final HttpResponse response = Post(url).execute().returnResponse();
            logger.info("Executed job id: {}, name : {}, url: {} ", id, name, url);
        } catch (IOException e) {
            logger.error("Error executing the job id: {}, name: {}, url: {} ", id, name, url);
        }
    }
}
