package net.p.i.scheduler.requests;

import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.URL;

public class JobRequest {

    @NotBlank(message="Name of the job is required")
    private String name;
    @NotBlank(message="Cron schedule is required")
    private String cronExpression;
    @NotBlank(message = "URL is required")
    @URL(message = "URL is invalid")
    private String url;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCronExpression() {
        return cronExpression;
    }

    public void setCronExpression(String cronExpression) {
        this.cronExpression = cronExpression;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
