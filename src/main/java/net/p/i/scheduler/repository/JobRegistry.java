package net.p.i.scheduler.repository;

import net.p.i.scheduler.domain.Job;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import static java.util.Optional.ofNullable;

@Component
public class JobRegistry {

    private final Map<String, Job> jobs = new HashMap<>();

    public Job register(Job job){
        return jobs.put(job.getId(), job);
    }

    public Optional<Job> getJobById(String id){
        return ofNullable(jobs.get(id));
    }

    public void remove(String id) {
        jobs.remove(id);
    }
}
