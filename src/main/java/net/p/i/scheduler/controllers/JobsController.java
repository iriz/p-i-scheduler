package net.p.i.scheduler.controllers;


import net.p.i.scheduler.domain.Job;
import net.p.i.scheduler.repository.JobRegistry;
import net.p.i.scheduler.requests.JobRequest;
import net.p.i.scheduler.tasks.builders.PostHttpJobBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.TaskScheduler;
import org.springframework.scheduling.support.CronTrigger;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.concurrent.ScheduledFuture;

import static java.util.stream.Collectors.toList;
import static org.apache.commons.lang3.RandomStringUtils.randomNumeric;
import static org.springframework.http.HttpStatus.*;
import static org.springframework.http.ResponseEntity.*;

@RestController
public class JobsController {

    @Autowired
    private TaskScheduler scheduler;
    @Autowired
    private JobRegistry jobRegistry;

    @PostMapping("/jobs")
    public ResponseEntity<?> createNewJob(@Valid @RequestBody JobRequest request, BindingResult errors){

        if(errors.hasErrors()){
            return badRequest().body(toErrorMessages(errors));
        }

        final String jobId = randomNumeric(5);
        final Job job = new Job(jobId,
                request.getName(), request.getUrl(), request.getCronExpression(), schedule(jobId, request));

        jobRegistry.register(job);

        return status(CREATED).body(job);
    }


    @PostMapping("/jobs/{id}/runNow")
    public ResponseEntity<?> runJobNow(@PathVariable("id") String id){
        return jobRegistry.getJobById(id)
                .map(j-> executeJob(j))
                .orElse(notFound().build());
    }

    @DeleteMapping("/jobs/{id}")
    public ResponseEntity<?> cancelJob(@PathVariable("id") String id){
        return jobRegistry.getJobById(id)
                .map(this::cancelJobSchedule)
                .orElse(status(NOT_FOUND).body("job not found"));
    }

    private ResponseEntity cancelJobSchedule(Job job) {
       if(job.getSchedule().cancel(false)){
           jobRegistry.remove(job.getId());
           return ok("Job successfully canceled");
       }
       return status(INTERNAL_SERVER_ERROR).body("Error canceling the schedule job");
    }

    private ResponseEntity executeJob(Job job) {
        new Thread(new PostHttpJobBuilder()
                .id(job.getId())
                .name(job.getName())
                .url(job.getUrl())
                .build()
        ).start();

        return accepted().build();
    }

    private ScheduledFuture<?> schedule(String id, JobRequest request){
        return scheduler.schedule(new PostHttpJobBuilder()
                        .id(id)
                        .name(request.getName())
                        .url(request.getUrl())
                        .build(),
                new CronTrigger(request.getCronExpression()));
    }

    private List<String> toErrorMessages(BindingResult errors) {
        return errors.getAllErrors()
                .stream().map(e -> e.getDefaultMessage())
                .collect(toList());
    }

}
