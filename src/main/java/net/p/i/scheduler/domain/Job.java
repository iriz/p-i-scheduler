package net.p.i.scheduler.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.concurrent.ScheduledFuture;

public class Job {

    private String id;
    private String name;
    private String url;
    private String cronExpression;
    private ScheduledFuture<?> schedule;

    public Job(@JsonProperty("id") String id,
               @JsonProperty("name") String name,
               @JsonProperty("url") String url,
               @JsonProperty("cronExpression") String cronExpression) {
        this(id, name, url, cronExpression, null);
    }

    public Job(String id, String name, String url, String cronExpression, ScheduledFuture<?> schedule) {
        this.id = id;
        this.name = name;
        this.url = url;
        this.cronExpression = cronExpression;
        this.schedule = schedule;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getUrl() {
        return url;
    }

    public String getCronExpression() {
        return cronExpression;
    }

    @JsonIgnore
    public ScheduledFuture<?> getSchedule() {
        return schedule;
    }
}
